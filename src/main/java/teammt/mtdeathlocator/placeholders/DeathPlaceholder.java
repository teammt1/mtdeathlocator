package teammt.mtdeathlocator.placeholders;

import org.bukkit.Location;
import org.bukkit.OfflinePlayer;

import masecla.mlib.classes.RegisterablePlaceholder;
import masecla.mlib.classes.Replaceable;
import masecla.mlib.main.MLib;
import teammt.mtdeathlocator.managers.DeathTracking;

public class DeathPlaceholder extends RegisterablePlaceholder {

	private DeathTracking deathTracking;

	public DeathPlaceholder(MLib lib, DeathTracking deathTracking) {
		super(lib);
		this.deathTracking = deathTracking;
	}

	@Override
	public String getIdentifier() {
		return "death";
	}

	@Override
	public String getPlaceholder(OfflinePlayer p, String target) {
		if (!p.isOnline())
			return null;

		Location loc = deathTracking.getLastDeathLocation(p.getPlayer());

		String msg = lib.getMessagesAPI().getPluginMessage("death-papi", new Replaceable("%x%", loc.getBlockX()),
				new Replaceable("%y%", loc.getBlockY()), new Replaceable("%z%", loc.getBlockZ()));

		return msg;
	}

}
