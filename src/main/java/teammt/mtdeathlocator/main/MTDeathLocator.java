package teammt.mtdeathlocator.main;

import org.bukkit.plugin.java.JavaPlugin;

import masecla.mlib.main.MLib;
import teammt.mtdeathlocator.commands.DeathCommand;
import teammt.mtdeathlocator.managers.DeathTracking;
import teammt.mtdeathlocator.placeholders.DeathPlaceholder;

public class MTDeathLocator extends JavaPlugin {

	private MLib lib;

	private DeathTracking deathTracking;

	@Override
	public void onEnable() {

		lib = new MLib(this);
		lib.getConfigurationAPI().requireAll();

		this.deathTracking = new DeathTracking(lib);
		this.deathTracking.register();

		new DeathCommand(lib, deathTracking).register();
		new DeathPlaceholder(lib, deathTracking).register();
	}
}
