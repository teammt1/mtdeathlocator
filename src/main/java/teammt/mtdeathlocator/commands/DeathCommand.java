package teammt.mtdeathlocator.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import masecla.mlib.annotations.RegisterableInfo;
import masecla.mlib.annotations.SubcommandInfo;
import masecla.mlib.classes.Registerable;
import masecla.mlib.classes.Replaceable;
import masecla.mlib.main.MLib;
import teammt.mtdeathlocator.managers.DeathTracking;

@RegisterableInfo(command = "death")
public class DeathCommand extends Registerable {

	private DeathTracking deathTracking;

	public DeathCommand(MLib lib, DeathTracking deathTracking) {
		super(lib);
		this.deathTracking = deathTracking;
	}

	@Override
	public void fallbackCommand(CommandSender sender, String[] args) {
		if (!sender.hasPermission("teammt.deathlocator.help")) {
			lib.getMessagesAPI().sendMessage("no-permission", sender);
			return;
		}

		lib.getMessagesAPI().sendMessage("help", sender);
	}

	@SubcommandInfo(subcommand = "reload", permission = "teammt.deathlocator.reload")
	public void handleReload(CommandSender sender) {
		lib.getConfigurationAPI().reloadAll();
		lib.getMessagesAPI().reloadSharedConfig();
		lib.getMessagesAPI().sendMessage("plugin-reloaded", sender);
	}

	@SubcommandInfo(subcommand = "find", permission = "teammt.deathlocator.find.self")
	public void handleFindSelf(Player pl) {
		handleFindSelf(pl, pl.getName());
	}

	@SubcommandInfo(subcommand = "find", permission = "teammt.deathlocator.find.other")
	public void handleFindSelf(Player pl, String target) {
		@SuppressWarnings("deprecation")
		OfflinePlayer trg = Bukkit.getOfflinePlayer(target);

		if (trg == null || !trg.hasPlayedBefore()) {
			lib.getMessagesAPI().sendMessage("invalid-player", pl);
			return;
		}

		Location l = deathTracking.getLastDeathLocation(trg.getPlayer());

		if (l == null) {
			lib.getMessagesAPI().sendMessage("no-death", pl);
			return;
		}

		lib.getMessagesAPI().sendMessage("your-location", pl, new Replaceable("%x%", l.getBlockX()),
				new Replaceable("%y%", l.getBlockY()), new Replaceable("%z%", l.getBlockZ()),
				new Replaceable("%world%", l.getWorld().getName()), new Replaceable("%player%", trg.getName()));

	}

	@SubcommandInfo(subcommand = "teleport", permission = "teammt.deathlocator.teleport.self")
	public void handleTeleport(Player player) {
		handleTeleport(player, player.getName());
	}

	@SubcommandInfo(subcommand = "teleport", permission = "teammt.deathlocator.teleport.others")
	public void handleTeleport(Player pl, String target) {
		@SuppressWarnings("deprecation")
		OfflinePlayer trg = Bukkit.getOfflinePlayer(target);

		if (trg == null || !trg.hasPlayedBefore()) {
			lib.getMessagesAPI().sendMessage("invalid-player", pl);
			return;
		}
		Location l = (Location) lib.getConfigurationAPI().getConfig("deaths")
				.get(trg.getUniqueId() + ".lastlocation", null);
		if (l == null) {
			lib.getMessagesAPI().sendMessage("no-death", pl);
			return;
		}
		lib.getMessagesAPI().sendMessage("teleported", pl);
		pl.teleport(l);
	}
}
