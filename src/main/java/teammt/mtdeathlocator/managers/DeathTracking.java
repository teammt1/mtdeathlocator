package teammt.mtdeathlocator.managers;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;

import masecla.mlib.classes.Registerable;
import masecla.mlib.main.MLib;

public class DeathTracking extends Registerable {

	public DeathTracking(MLib lib) {
		super(lib);
	}

	public void setLastDeathLocation(Player player, Location location) {
		lib.getConfigurationAPI().getConfig("deaths").set(player.getUniqueId() + ".deathLocation", location);
	}

	public Location getLastDeathLocation(Player player) {
		return (Location) lib.getConfigurationAPI().getConfig("deaths")
				.get(player.getUniqueId() + ".deathLocation", null);
	}

	@EventHandler
	public void handlePlayerDeath(PlayerDeathEvent ev) {
		setLastDeathLocation(ev.getEntity(), ev.getEntity().getLocation());
	}
}
